#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "graph.h"

#define MAX_BUFFER_SIZE 1000
/*

cour fichier.C/H 
regarger le bas partie cours.


*/

// Fonction qui va lire les lignes du fichier : metros.txt
void lireLigne(Graph* graph, char* ligne)
{
    char* debut; 
    char* fin; 
    char* station; 
    char* StationVu; 
    char* nomLigne;
    char* temps_str;
    int temps;
    unsigned int taille; 


    debut = strchr(ligne, '(');
    debut++; // commence alr au numéro de correspondance 
    fin = strchr(ligne, ')'); // commence à ) direct 

    taille = fin - debut;// il reste 1 element : 3 qui n'est pas dans fin . logique enlever debut de fin il reste donc 3 de debut

    temps_str = (char*)(malloc(sizeof(char) * 10));
    temps_str[taille] = '\0'; // chaine à 2 élément vlr 0 et 1 = \n
    
    strncpy(temps_str, debut, taille); // on copie le temps dans la chaine 

    temps = atoi(temps_str); // transforme la vlr de la chaine en entier int

    debut = ++fin; // commence direct a L1.. au lieu de ) 

    fin = strchr(ligne, ':'); // commence au : 

    taille = fin - debut; // on obtien les element restant (4 , en comptant les espace)

    nomLigne = (char*)(malloc(sizeof(char) * 10)); // on alloue de l'espace 

    strncpy(nomLigne, debut, taille); // on copie la ligne dans la chaine 

    nomLigne[taille] = '\0'; // dernier élément = fin 

    debut = ++fin; // affiche pour voir debut comporte quoi 
    fin = strstr(debut, "<->"); // fin = <->Esplanade de La Defense<->Pont de Neuilly
    taille = fin - debut;// afiche pour voir nb 



    StationVu = NULL;

    while (fin != NULL) // fin = chaine qui contient tout les station de la ligne 
    {
        
        station = (char*)(malloc(sizeof(char) * 50));
		

        strncpy(station, debut, taille); // savoir ce qui est copier dans station 

        station[taille] = '\0';


        if (StationVu != NULL) // a chaque fois qu'il est != nul on fais lien avec la station derniere
        {
            ajouterLien(graph, StationVu, station, nomLigne, temps);
        }


        debut = fin + 3;// Esplanade de La Defense<->Pont de Neuilly (sans <->)
        fin = strstr(debut, "<->"); //<->Pont de Neuilly
        taille = fin - debut;//23

        if (fin == NULL) // quand on arrivent à la derniere chaine on fais aussi lien avec la station d'avant
        {
            taille = strlen(debut);
            strncpy(station, debut, taille);
            station[taille] = '\0';
            ajouterLien(graph, StationVu, station, nomLigne, temps);
        }

		free(StationVu);
        StationVu = (char *)(malloc(sizeof(char) * strlen(station) + 1)); // aloue de l'espace

        
        strcpy(StationVu, station);
		free(station);
		
    }
// libère tout la mémoire alloué 
	free(StationVu);
	free(temps_str);
    free(nomLigne);
}

//Ouvrir fichier metros dans l'onglet 
//Fonction qui permet d'entrer les stations 

Station* lireStation(Graph * g) 
{
    char* tmp = (char*)(malloc(sizeof(char) * 50));

    fgets(tmp, 50, stdin); 
    tmp[strcspn(tmp, "\r\n")] = 0; 

    int index = rechercherStationParNom(g, tmp); 

    while (index == -1) 
    {
        printf("La station n'existe pas , veuillez reessayer\n"); 
        
        fgets(tmp, 50, stdin); 
        tmp[strcspn(tmp, "\r\n")] = 0; 
        index = rechercherStationParNom(g, tmp);
    }

	free(tmp);
	
    return g->stations[index]; 
}

int main(int argc, char * argv[]){
  
    FILE* fichier;
    Station* depart;
    Station* arrivee;

    fichier = fopen("metros.txt", "r");
    if (fichier == NULL) {
        printf("Erreur\n");
    
    }

    Graph* graph = initGraph();
	
// Lecture de metros.txt ligne/ligne 
    while (!feof(fichier)) 
    {
        char* buffer = (char*)(malloc(sizeof(char) * MAX_BUFFER_SIZE));

        fgets(buffer, MAX_BUFFER_SIZE, fichier);

        lireLigne(graph, buffer); 
		
		free(buffer);
    }
    
    
    printf("Veuillez entrer la station de depart\n");
    depart = lireStation(graph);
    printf("Veuillez entrer la station d'arrivee\n");
    arrivee = lireStation(graph);
    
    Station ** chemin = dijkstra(graph, depart, arrivee);  

    dijkstra_chemin(graph, depart, arrivee, chemin); 

    free(chemin);  

    nettoie_Graph(graph);
	
	free(graph);

    fclose(fichier);

	return 0;
}